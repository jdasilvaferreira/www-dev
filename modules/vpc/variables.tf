variable "cidr_block" {}
variable "destination_cidr_block" {
    default = "0.0.0.0/0"
}